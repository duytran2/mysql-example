<?php
// DIR config
define('DIR_BASE', dirname(dirname(__FILE__)) . '/');
define('DIR_SYSTEM', DIR_BASE . 'mysql_example/');  //change
define('DIR_ASSETS', DIR_SYSTEM . 'assets/');
define('DIR_VIEWS', DIR_ASSETS . 'views/');
define('DIR_FUNCTION', DIR_ASSETS . 'function/');
define('DIR_PARTIALS', DIR_VIEWS . 'partials/');
define('DIR_CSS', DIR_ASSETS . 'css/');
define('VIEW_HEADER', DIR_PARTIALS . 'header.php');

// URL config
define('URL_BASE', 'http://' . $_SERVER['HTTP_HOST'] . '/');
define('URL_SYSTEM', URL_BASE . 'mysql_example/');  //change
define('URL_PUBLIC', URL_SYSTEM . 'public/');
define('URL_ASSETS', URL_SYSTEM . 'assets/');
define('URL_VIEWS', URL_ASSETS . 'views/');
define('URL_PARTIALS', URL_VIEWS . 'partials/');
define('URL_CSS', URL_ASSETS . 'css/');

// Database config
define('SERVER_NAME', 'localhost');
define('PORT_NUMBER', 3306);
define('DB_NAME', 'example1_mysql_db');
define('USER_NAME', 'root');
define('PASSWORD', '');
