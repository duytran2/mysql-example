-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2022 at 12:40 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `example1_mysql_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cthd`
--

CREATE TABLE `cthd` (
  `id` int(11) NOT NULL,
  `sohd` int(11) NOT NULL,
  `masp` char(4) DEFAULT NULL,
  `sl` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cthd`
--

INSERT INTO `cthd` (`id`, `sohd`, `masp`, `sl`) VALUES
(1, 1001, 'TV02', 10),
(2, 1001, 'ST01', 5),
(3, 1001, 'BC01', 5),
(4, 1001, 'BC02', 10),
(5, 1001, 'ST08', 10),
(6, 1002, 'BC04', 20),
(7, 1002, 'BB01', 20),
(8, 1002, 'BB02', 20),
(14, 1005, 'TV05', 50),
(15, 1005, 'TV06', 50),
(16, 1006, 'TV07', 20),
(17, 1006, 'ST01', 30),
(18, 1006, 'ST02', 10),
(19, 1007, 'ST03', 10),
(20, 1008, 'ST04', 8),
(21, 1009, 'ST05', 10),
(22, 1010, 'TV07', 50),
(23, 1010, 'ST07', 50),
(24, 1010, 'ST08', 100),
(25, 1010, 'ST04', 50),
(26, 1010, 'TV03', 100),
(27, 1011, 'ST06', 50),
(28, 1012, 'ST07', 35),
(29, 1013, 'ST08', 5),
(30, 1014, 'BC02', 80),
(31, 1014, 'BB02', 100),
(32, 1014, 'BC04', 60),
(33, 1014, 'BB01', 50),
(34, 1015, 'BB02', 30),
(35, 1015, 'BB03', 7),
(36, 1016, 'TV01', 5),
(37, 1017, 'TV02', 1),
(38, 1017, 'TV03', 1),
(39, 1017, 'TV04', 5),
(40, 1018, 'ST04', 6),
(41, 1019, 'ST05', 1),
(42, 1019, 'ST06', 2),
(43, 1020, 'ST07', 10),
(44, 1021, 'ST08', 5),
(45, 1021, 'TV01', 7),
(46, 1021, 'TV02', 10),
(47, 1022, 'ST07', 1),
(48, 1023, 'ST04', 6);

-- --------------------------------------------------------

--
-- Table structure for table `hoadon`
--

CREATE TABLE `hoadon` (
  `sohd` int(11) NOT NULL,
  `nghd` date DEFAULT NULL,
  `makh` char(4) DEFAULT NULL,
  `manv` char(4) DEFAULT NULL,
  `trigia` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hoadon`
--

INSERT INTO `hoadon` (`sohd`, `nghd`, `makh`, `manv`, `trigia`) VALUES
(1007, '2006-10-28', 'KH03', 'NV03', 510000),
(1009, '2006-10-28', 'KH03', 'NV04', 200000),
(1011, '2006-11-04', 'KH04', 'NV03', 250000),
(1012, '2006-11-30', 'KH05', 'NV03', 21000),
(1013, '2006-12-12', 'KH06', 'NV01', 5000),
(1014, '2006-12-31', 'KH03', 'NV02', 3150000),
(1015, '2007-01-01', 'KH06', 'NV01', 910000),
(1020, '2007-01-14', 'KH09', 'NV04', 70000),
(1021, '2007-01-16', 'KH10', 'NV03', 67500),
(1022, '2007-01-16', NULL, 'NV03', 7000),
(1023, '2007-01-17', NULL, 'NV01', 330000);

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE `khachhang` (
  `makh` char(4) NOT NULL,
  `hoten` varchar(40) DEFAULT NULL,
  `dchi` varchar(50) DEFAULT NULL,
  `sodt` varchar(20) DEFAULT NULL,
  `ngsinh` date DEFAULT NULL,
  `ngdk` date DEFAULT NULL,
  `doanhso` int(11) DEFAULT NULL,
  `loaikh` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`makh`, `hoten`, `dchi`, `sodt`, `ngsinh`, `ngdk`, `doanhso`, `loaikh`) VALUES
('KH01', 'Nguyen Van B', '731 Tran Hung Dao, Q5, TpHCM', '08823451', '1960-10-22', '2006-07-22', 13060000, NULL),
('KH03', 'Tran Ngoc Linh', '45 Nguyen Canh Chan, Q1, TpHCM', '0938776266', '1980-06-12', '2006-08-05', 3860000, NULL),
('KH04', 'Tran Minh Long', '50/34 Le Dai Hành, Q10, TpHCM', '0917325476', '1965-03-09', '2006-10-02', 250000, NULL),
('KH05', 'Le Nhat Minh', '34 Truong Dinh, Q3, TpHCM', '08246108', '1950-03-10', '2006-10-28', 21000, NULL),
('KH06', 'Le Hoai Thuong', '227 Nguyen Van Cu, Q5, TpHCM', '08631738', '1981-12-31', '2006-11-24', 915000, NULL),
('KH09', 'Nguyen Van Hoan', '873 Le Hong Phong, Q5, TpHCM', '08654763', '1979-09-03', '2007-01-14', 70000, NULL),
('KH10', 'Ha Duy Lap', '34/34B Nguyen Trai, Q1, TpHCM', '08768904', '1983-05-02', '2007-01-16', 67500, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE `nhanvien` (
  `manv` char(4) NOT NULL,
  `hoten` varchar(40) DEFAULT NULL,
  `sodt` varchar(20) DEFAULT NULL,
  `ngvl` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`manv`, `hoten`, `sodt`, `ngvl`) VALUES
('NV01', 'Nguyen Nhu Nhut', '0927345678', '2006-04-13'),
('NV02', 'Le Thi Phi Yen', '0987567390', '2006-04-21'),
('NV03', 'Nguyen Van B', '0997047382', '2006-04-13'),
('NV04', 'Ngo Thanh Tuan', '0913758498', '2006-06-24'),
('NV05', 'Nguyen Thi Truc Thanh', '0918590387', '2006-07-20');

-- --------------------------------------------------------

--
-- Table structure for table `sanpham`
--

CREATE TABLE `sanpham` (
  `masp` char(4) NOT NULL,
  `tensp` varchar(40) DEFAULT NULL,
  `dvt` varchar(20) DEFAULT NULL,
  `nuocsx` varchar(40) DEFAULT NULL,
  `gia` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sanpham`
--

INSERT INTO `sanpham` (`masp`, `tensp`, `dvt`, `nuocsx`, `gia`) VALUES
('BB01', 'But bi', 'cay', 'Singapore', 5000),
('BB02', 'But bi', 'cay', 'Trung Quoc', 7000),
('BB03', 'But bi', 'hop', 'Thai Lan', 100000),
('BC01', 'But chi', 'cay', 'Singapore', 3000),
('BC02', 'But chi', 'cay', 'Singapore', 5000),
('BC03', 'But chi', 'cay', 'Viet Nam', 3500),
('BC04', 'But chi', 'hop', 'Viet Nam', 30000),
('ST01', 'So tay 500 trang', 'quyen', 'Trung Quoc', 40000),
('ST02', 'So tay loai 1', 'quyen', 'Viet Nam', 55000),
('ST03', 'So tay loai 2', 'quyen', 'Viet Nam', 51000),
('ST04', 'So tay ', 'quyen', 'Thai Lan', 35000),
('ST05', 'So tay mong', 'quyen', 'Thai Lan', 20000),
('ST06', 'Phan viet bang', 'hop', 'Viet Nam', 5000),
('ST07', 'Phan khong bui', 'hop', 'Viet Nam', 7000),
('ST08', 'Bong bang', 'cai', 'Viet Nam', 1000),
('ST09', 'But long', 'cay', 'Viet Nam', 5000),
('ST10', 'But long', 'cay', 'Trung Quoc', 7000),
('TV01', 'Tap 100 giay mong', 'quyen', 'Trung Quoc', 2500),
('TV02', 'Tap 200 giay mong', 'quyen', 'Trung Quoc', 4500),
('TV03', 'Tap 100 giay tot', 'quyen', 'Viet Nam', 3000),
('TV04', 'Tap 200 giay tot', 'quyen', 'Viet Nam', 5500),
('TV05', 'Tap 100 trang', 'chuc', 'Viet Nam', 23000),
('TV06', 'Tap 200 trang', 'chuc', 'Viet Nam', 53000),
('TV07', 'Tap 100 trang', 'chuc', 'Trung Quoc', 34000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cthd`
--
ALTER TABLE `cthd`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_masp` (`masp`),
  ADD KEY `fk_sohd` (`sohd`);

--
-- Indexes for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD PRIMARY KEY (`sohd`),
  ADD KEY `fk_makh` (`makh`),
  ADD KEY `fk_manv` (`manv`);

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`makh`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`manv`);

--
-- Indexes for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`masp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cthd`
--
ALTER TABLE `cthd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cthd`
--
ALTER TABLE `cthd`
  ADD CONSTRAINT `fk_masp` FOREIGN KEY (`masp`) REFERENCES `sanpham` (`masp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sohd` FOREIGN KEY (`sohd`) REFERENCES `hoadon` (`sohd`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD CONSTRAINT `fk_makh` FOREIGN KEY (`makh`) REFERENCES `khachhang` (`makh`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_manv` FOREIGN KEY (`manv`) REFERENCES `nhanvien` (`manv`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
