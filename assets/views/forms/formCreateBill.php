<?php
$customers = require_once DIR_FUNCTION . '/customers/getAllCustomers.php';
$staffs = require_once DIR_FUNCTION . '/staffs/getAllStaffs.php';
$products = require_once DIR_FUNCTION . '/products/getAllProduct.php';

?>

<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php foreach ($errors as $error) : ?>
            <div><?php echo $error; ?></div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<form method="POST" action="" enctype="multipart/form-data">
    <!-- Bill number -->
    <div class="mb-3">
        <label class="form-label">Số hóa đơn</label>
        <?php if (strpos($_SERVER['SCRIPT_NAME'], 'update.php') !== false) : ?>
            <input name="" type="number" disabled min="0" max="9999" class="form-control" value="<?php echo $bill['sohd'] ?>">
            <input name="billNumber" type="hidden" min="0" max="9999" class="form-control" value="<?php echo $bill['sohd'] ?>">
        <?php else : ?>
            <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength="4" name="billNumber" type="number" min="0" max="9999" class="form-control" value="<?php echo $billNumber ?>">
        <?php endif; ?>
    </div>
    <!-- Bill number -->

    <!-- Customer -->
    <label class="form-label">Khách hàng</label>
    <select name="customerCode" class="form-select form-select" aria-label=".form-select example">
        <?php if (isset($bill)) : ?>
            <!-- layout update bill [khachhang]-->
            <option selected value=""></option>
            <?php foreach ($customers as $customer) : ?>
                <option <?php if (in_array($customer['makh'], $bill)) {
                            echo 'selected';
                        } ?> value="<?php echo $customer['makh'] ?>"><?php echo $customer['hoten'] ?></option>
            <?php endforeach; ?>
            <!-- layout update bill [khachhang]-->
        <?php else : ?>
            <!-- layout insert bill [khachhang]-->
            <option selected value=""></option>
            <?php foreach ($customers as $customer) : ?>
                <option value="<?php echo $customer['makh'] ?>"><?php echo $customer['hoten'] ?></option>
            <?php endforeach; ?>
            <!-- layout insert bill [khachhang]-->
        <?php endif; ?>
    </select>
    <!-- Customer -->

    <!-- Staff -->
    <label class="form-label">Nhân viên</label>
    <select name="staffCode" class="form-select form-select" aria-label=".form-select example">
        <?php if (isset($bill)) : ?>

            <!-- layout update bill [nhanvien]-->
            <option selected value=""></option>
            <?php foreach ($staffs as $staff) : ?>
                <option <?php if (in_array($staff['manv'], $bill)) {
                            echo 'selected';
                        } ?> value="<?php echo $staff['manv'] ?>"><?php echo $staff['hoten'] ?></option>
            <?php endforeach; ?>
            <!-- layout update bill [nhanvien]-->

        <?php else : ?>

            <!-- layout insert bill [nhanvien]-->
            <option selected value=""></option>
            <?php foreach ($staffs as $staff) : ?>
                <option value="<?php echo $staff['manv'] ?>"><?php echo $staff['hoten'] ?></option>
            <?php endforeach; ?>
            <!-- layout insert bill [nhanvien]-->

        <?php endif; ?>
    </select>
    <!-- Staff -->

    <!-- Product -->
    <label class="form-label">Sản phẩm</label>
    <br>
    <?php foreach ($products as $product) : ?>
        <div class="form-check product-check">
            <?php if (isset($billDetail)) : ?>

                <input <?php
                        foreach ($billDetail as $detail) {
                            if (in_array($product['masp'], $detail)) {
                                echo 'checked';
                            }
                        }
                        ?> name="product[]" class="form-check-input" type="checkbox" value="<?php echo $product['masp'] ?>"><?php echo $product['tensp'] . ' ' . $product['masp'] ?>
                <input value="<?php
                                foreach ($billDetail as $detail) {
                                    if (in_array($product['masp'], $detail)) {
                                        echo $detail['sl'];
                                    }
                                }
                                ?>" name="quantity[<?php echo $product['masp'] ?>]" type="number" placeholder="Số lượng" min="0" class="form-control product-quantity">

            <?php else : ?>

                <input name="product[]" class="form-check-input" type="checkbox" value="<?php echo $product['masp'] ?>"><?php echo $product['tensp'] . ' ' . $product['masp'] ?>
                <input name="quantity[<?php echo $product['masp'] ?>]" type="number" placeholder="Số lượng" min="0" class="form-control product-quantity">

            <?php endif; ?>
        </div>
    <?php endforeach; ?>
    <!-- Product -->

    <!-- Create Date -->
    <div class="mb-3">
        <label class="form-label">Ngày tạo</label>
        <input name="createDate" type="date" class="form-control" value="<?php echo $createDate; ?>">
    </div>
    <!-- Create Date -->

    <!-- Value -->
    <div class="mb-3">
        <label class="form-label">Trị giá</label>
        <input name="invoiceValue" type="number" step="100" class="form-control" value="<?php echo $invoiceValue;  ?>">
    </div>
    <!-- Value -->

    <button type=" submit" class="btn btn-primary">Submit</button>
</form>