<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php foreach ($errors as $error) : ?>
            <div><?php echo $error; ?></div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<form method="POST" action="" enctype="multipart/form-data">
    <div class="mb-3">
        <label class="form-label">Mã sản phẩm</label>
        <?php if (strpos($_SERVER['SCRIPT_NAME'], 'update.php') !== false) : ?>
            <input name="" maxlength="4" type="text" disabled class="form-control" value="<?php echo $code ?>">
            <input name="code" maxlength="4" type="hidden" class="form-control" value="<?php echo $code ?>">
        <?php else : ?>
            <input name="code" maxlength="4" type="text" class="form-control" value="<?php echo $code ?>">
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label class="form-label">Tên sản phẩm</label>
        <input name="title" type="text" class="form-control" value="<?php echo $title ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Đơn vị tính</label>
        <input name="unit" type="text" class="form-control" value="<?php echo $unit ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Nước sản xuất</label>
        <input name="country" type="text" class="form-control" value="<?php echo $country ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Giá</label>
        <input name="price" type="number" step="100" class="form-control" value="<?php echo $price ?>">
    </div>
    <button type=" submit" class="btn btn-primary">Submit</button>
</form>