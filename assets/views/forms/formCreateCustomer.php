<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php foreach ($errors as $error) : ?>
            <div><?php echo $error; ?></div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<form method="POST" action="" enctype="multipart/form-data">
    <div class="mb-3">
        <label class="form-label">Mã khách hàng</label>
        <?php if (strpos($_SERVER['SCRIPT_NAME'], 'update.php') !== false) : ?>
            <input name="" maxlength="4" type="text" disabled class="form-control" value="<?php echo $code ?>">
            <input name="code" maxlength="4" type="hidden" class="form-control" value="<?php echo $code ?>">
        <?php else : ?>
            <input name="code" maxlength="4" type="text" class="form-control" value="<?php echo $code ?>">
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label class="form-label">Tên khách hàng</label>
        <input name="name" type="text" class="form-control" value="<?php echo $name ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Địa chỉ</label>
        <input name="address" type="text" class="form-control" value="<?php echo $address ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Số điện thoại</label>
        <input name="phoneNumber" type="tel" class="form-control" value="<?php echo $phoneNumber ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Ngày sinh</label>
        <input name="birthday" type="date" class="form-control" value="<?php echo $birthday ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Ngày đăng ký</label>
        <input name="registerDay" type="date" class="form-control" value="<?php echo $registerDay ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Doanh số</label>
        <input name="purchase" type="number" step="100" class="form-control" value="<?php echo $purchase ?>">
    </div>
    <button type=" submit" class="btn btn-primary">Submit</button>
</form>