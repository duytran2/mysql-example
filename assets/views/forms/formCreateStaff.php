<?php if (!empty($errors)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php foreach ($errors as $error) : ?>
            <div><?php echo $error; ?></div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<form method="POST" action="" enctype="multipart/form-data">
    <div class="mb-3">
        <label class="form-label">Mã nhân viên</label>
        <?php if (strpos($_SERVER['SCRIPT_NAME'], 'update.php') !== false) : ?>
            <input name="" maxlength="4" type="text" disabled class="form-control" value="<?php echo $code ?>">
            <input name="code" maxlength="4" type="hidden" class="form-control" value="<?php echo $code ?>">
        <?php else : ?>
            <input name="code" maxlength="4" type="text" class="form-control" value="<?php echo $code ?>">
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <label class="form-label">Tên nhân viên</label>
        <input name="name" type="text" class="form-control" value="<?php echo $name ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Số điện thoại</label>
        <input name="phoneNumber" type="tel" class="form-control" value="<?php echo $phoneNumber ?>">
    </div>
    <div class="mb-3">
        <label class="form-label">Ngày vào làm</label>
        <input name="workDay" type="date" class="form-control" value="<?php echo $workDay ?>">
    </div>
    <button type=" submit" class="btn btn-primary">Submit</button>
</form>