new WOW({
    live: true,
    animateClass: 'animate__animated'
}).init()

$('.card').hover(function () {
    var card = $(this)
    card.toggleClass('animate__animated animate__pulse')
})