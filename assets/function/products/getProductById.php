<?php
require_once('../../conn.php');
require_once('../../assets/function/randomString.php');

$id = $_GET['id'] ?? null;

if (!$id) {
    header('Location: index.php');
    exit;
}

$statement = $conn->prepare('SELECT * FROM sanpham WHERE masp = :id');
$statement->bindValue(':id', $id);
$statement->execute();
$product = $statement->fetch(PDO::FETCH_ASSOC);

return $product;
