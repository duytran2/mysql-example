<?php

// store POST data
$code = $_POST['code'];
$title = $_POST['title'];
$unit = $_POST['unit'];
$country = $_POST['country'];
$price = $_POST['price'];

// form validate
if (!$code) {
    $errors[] = 'Vui lòng nhập mã sản phẩm';
}
if (!$title) {
    $errors[] = 'Vui lòng nhập tên sản phẩm';
}
if (!$unit) {
    $errors[] = 'Vui lòng nhập đơn vị tính';
}
if (!$country) {
    $errors[] = 'Vui lòng nhập nước sản xuất';
}
if (!$price) {
    $errors[] = 'Vui lòng nhập giá sản phẩm';
}
