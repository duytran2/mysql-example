<?php
require_once('../../conn.php');

$statement = $conn->prepare('SELECT * FROM sanpham');
$statement->execute();
$products = $statement->fetchAll(PDO::FETCH_ASSOC);

return $products;
