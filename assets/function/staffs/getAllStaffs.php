<?php
require_once('../../conn.php');

$statement = $conn->prepare('SELECT * FROM nhanvien');
$statement->execute();
$staffs = $statement->fetchAll(PDO::FETCH_ASSOC);

return $staffs;
