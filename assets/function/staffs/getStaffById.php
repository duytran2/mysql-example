<?php
require_once('../../conn.php');

$id = $_GET['id'] ?? null;

if (!$id) {
    header('Location: index.php');
    exit;
}

$statement = $conn->prepare('SELECT * FROM nhanvien WHERE manv = :manv');
$statement->bindValue(':manv', $id);
$statement->execute();
$staff = $statement->fetch(PDO::FETCH_ASSOC);

return $staff;
