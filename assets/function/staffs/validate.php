<?php

// store POST data
$code = $_POST['code'];
$name = $_POST['name'];
$phoneNumber = $_POST['phoneNumber'];
$workDay = $_POST['workDay'];

// form validate
if (!$code) {
    $errors[] = 'Vui lòng nhập mã nhân viên';
}
if (!$name) {
    $errors[] = 'Vui lòng nhập tên nhân viên';
}
if (!$phoneNumber) {
    $errors[] = 'Vui lòng nhập số điện thoại';
}
if (!$workDay) {
    $errors[] = 'Vui lòng ngày vào làm';
}
