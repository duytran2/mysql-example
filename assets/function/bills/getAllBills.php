<?php
require_once('../../conn.php');

$statement = $conn->prepare('SELECT hd.sohd, hd.nghd, kh.hoten AS tenkh, nv.hoten as tennv, hd.trigia
FROM hoadon AS hd LEFT OUTER JOIN khachhang AS kh ON hd.makh = kh.makh JOIN nhanvien as nv ON hd.manv = nv.manv

UNION

SELECT hd.sohd, hd.nghd, kh.hoten AS tenkh, nv.hoten as tennv, hd.trigia
FROM hoadon AS hd JOIN khachhang AS kh ON hd.makh = kh.makh JOIN nhanvien AS nv ON hd.manv = nv.manv

ORDER BY sohd ASC');

$statement->execute();

$bills = $statement->fetchAll(PDO::FETCH_ASSOC);

return $bills;
