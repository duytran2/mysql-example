<?php
require_once('../../conn.php');

$id = $_GET['id'] ?? null;

if (!$id) {
    header('Location: index.php');
    exit;
}

$statement = $conn->prepare('SELECT * FROM cthd WHERE sohd = :sohd');

$statement->bindValue(':sohd', $id);

$statement->execute();

$billDetail = $statement->fetchAll(PDO::FETCH_ASSOC);

return $billDetail;
