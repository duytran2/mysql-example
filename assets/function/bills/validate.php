<?php

// store POST data

$billNumber = $_POST['billNumber'];
$customerCode = $_POST['customerCode'];
$staffCode = $_POST['staffCode'];
$createDate = $_POST['createDate'];
$invoiceValue = $_POST['invoiceValue'];

$products = $_POST['product'] ?? null;
$quantity = $_POST['quantity'] ?? null;

// form validate
if (!$billNumber) {
    $errors[] = 'Vui lòng nhập số hóa đơn';
}
if (!$customerCode) {
    $errors[] = 'Vui lòng nhập khách hàng';
}
if (!$staffCode) {
    $errors[] = 'Vui lòng nhập nhân viên';
}
if (!$createDate) {
    $errors[] = 'Vui lòng nhập ngày mua';
}
// if (!$phoneNumber) {
//     $errors[] = 'Vui lòng nhập số điện thoại';
// }
// if (!$purchase) {
//     $errors[] = 'Vui lòng nhập doanh số';
// }
