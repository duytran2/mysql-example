<?php
require_once('../../conn.php');

$id = $_GET['id'] ?? null;

if (!$id) {
    header('Location: index.php');
    exit;
}

$statement = $conn->prepare('SELECT hd.sohd, hd.nghd, hd.makh, hd.manv, kh.hoten AS tenkh, nv.hoten as tennv, hd.trigia
FROM hoadon AS hd LEFT OUTER JOIN khachhang AS kh ON hd.makh = kh.makh JOIN nhanvien as nv ON hd.manv = nv.manv
WHERE sohd = :sohd

UNION

SELECT hd.sohd, hd.nghd, hd.makh, hd.manv, kh.hoten AS tenkh, nv.hoten as tennv, hd.trigia
FROM hoadon AS hd JOIN khachhang AS kh ON hd.makh = kh.makh JOIN nhanvien AS nv ON hd.manv = nv.manv
WHERE sohd = :sohd');

$statement->bindValue(':sohd', $id);

$statement->execute();

$bill = $statement->fetch(PDO::FETCH_ASSOC);

return $bill;
