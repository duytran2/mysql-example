<?php
require_once('../../conn.php');

$statement = $conn->prepare('SELECT * FROM khachhang');
$statement->execute();
$customers = $statement->fetchAll(PDO::FETCH_ASSOC);

return $customers;
