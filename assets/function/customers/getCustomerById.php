<?php
require_once('../../conn.php');
require_once('../../assets/function/randomString.php');

$id = $_GET['id'] ?? null;

if (!$id) {
    header('Location: index.php');
    exit;
}

$statement = $conn->prepare('SELECT * FROM khachhang WHERE makh = :makh');
$statement->bindValue(':makh', $id);
$statement->execute();
$customers = $statement->fetch(PDO::FETCH_ASSOC);

return $customers;
