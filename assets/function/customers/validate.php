<?php

// store POST data
$code = $_POST['code'];
$name = $_POST['name'];
$address = $_POST['address'];
$phoneNumber = $_POST['phoneNumber'];
$birthday = $_POST['birthday'];
$registerDay = $_POST['registerDay'];
$purchase = $_POST['purchase'];

// form validate
if (!$code) {
    $errors[] = 'Vui lòng nhập mã khách hàng';
}
if (!$name) {
    $errors[] = 'Vui lòng nhập tên khách hàng';
}
if (!$phoneNumber) {
    $errors[] = 'Vui lòng nhập số điện thoại';
}
if (!$purchase) {
    $errors[] = 'Vui lòng nhập doanh số';
}
