<?php
$staff = require_once('../../assets/function/staffs/getStaffById.php');

$errors = [];

// define empty data
$code = $staff['manv'];
$name = $staff['hoten'];
$phoneNumber = $staff['sodt'];
$workDay = $staff['ngvl'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once('../../assets/function/staffs/validate.php');

    // if no error
    if (empty($errors)) {

        // update to database table
        $statement = $conn->prepare("UPDATE nhanvien SET hoten = :hoten, sodt = :sodt, ngvl = :ngvl WHERE manv = :manv");

        $statement->bindValue(':hoten', $name);
        $statement->bindValue(':sodt', $phoneNumber);
        $statement->bindValue(':ngvl', $workDay);
        $statement->bindValue(':manv', $code);

        $statement->execute();
    }
}
include_once('../../config.php');
require_once VIEW_HEADER;
?>
<div class="wrap">

    <h1>Staff CRUD</h1>
    <p>
        <a href="index.php" type="button" class="btn btn-sm btn-secondary ">Back</a>
    </p>

    <?php require_once('../../assets/views/forms/formCreateStaff.php') ?>

</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>