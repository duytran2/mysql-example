<?php
require_once('../../conn.php');

$errors = [];

// define empty data
$code = '';
$name = '';
$phoneNumber = '';
$workday = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once('../../assets/function/staffs/validate.php');

    // if no error
    if (empty($errors)) {
        $statement = $conn->prepare("SELECT * FROM nhanvien WHERE manv = :manv");
        $statement->bindValue(':manv', $code);
        $statement->execute();

        $row = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$row) {
            // insert to database table
            $statement = $conn->prepare("INSERT INTO nhanvien (manv, hoten, sodt, ngvl) VALUES (:manv, :hoten, :sodt, :ngvl)");

            $statement->bindValue(':manv', $code);
            $statement->bindValue(':hoten', $name);
            $statement->bindValue(':sodt', $phoneNumber);
            $statement->bindValue(':ngvl', $workday);

            $statement->execute();

            // Reset data
            $code = '';
            $name = '';
            $phoneNumber = '';
            $workday = '';
        } else {
            $errors[] = 'Mã nhân viên đã tồn tại';
        }
    }
}
include_once('../../config.php');
require_once VIEW_HEADER;
?>
<div class="wrap">

    <h1>Staff CRUD</h1>
    <p>
        <a href="index.php" type="button" class="btn btn-sm btn-secondary ">Back</a>
    </p>

    <?php require_once('../../assets/views/forms/formCreateStaff.php') ?>

</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>