<?php
$customer = require_once('../../assets/function/customers/getCustomerById.php');

$errors = [];

// define empty data
$code = $customer['makh'];
$name = $customer['hoten'];
$address = $customer['dchi'];
$phoneNumber = $customer['sodt'];
$birthday = $customer['ngsinh'];
$registerDay = $customer['ngdk'];
$purchase = $customer['doanhso'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once('../../assets/function/customers/validate.php');

    // if no error
    if (empty($errors)) {
        $statement = $conn->prepare("UPDATE khachhang SET hoten = :hoten, dchi = :dchi, sodt = :sodt, ngsinh = :ngsinh, ngdk = :ngdk, doanhso = :doanhso WHERE makh = :makh");

        $statement->bindValue(':makh', $code);
        $statement->bindValue(':hoten', $name);
        $statement->bindValue(':dchi', $address);
        $statement->bindValue(':sodt', $phoneNumber);
        $statement->bindValue(':ngsinh', $birthday);
        $statement->bindValue(':ngdk', $registerDay);
        $statement->bindValue(':doanhso', $purchase);

        $statement->execute();
    }
}
include_once('../../config.php');
require_once VIEW_HEADER;
?>
<div class="wrap">

    <h1>Customer CRUD</h1>
    <p>
        <a href="index.php" type="button" class="btn btn-sm btn-secondary ">Back</a>
    </p>

    <?php require_once('../../assets/views/forms/formCreateCustomer.php') ?>

</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>