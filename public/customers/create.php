<?php
require_once('../../conn.php');
require_once('../../assets/function/randomString.php');

$errors = [];

// define empty data
$code = '';
$name = '';
$address = '';
$phoneNumber = '';
$birthday = '';
$registerDay = '';
$purchase = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once('../../assets/function/customers/validate.php');

    // if no error
    if (empty($errors)) {
        $statement = $conn->prepare("SELECT * FROM khachhang WHERE makh = :makh");
        $statement->bindValue(':makh', $code);
        $statement->execute();

        $row = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$row) {
            // insert to database table
            $statement = $conn->prepare("CALL insertCustomer (?, ?, ?, ?, ?, ?, ?)");

            $statement->bindParam(1, $code);
            $statement->bindParam(2, $name);
            $statement->bindParam(3, $address);
            $statement->bindParam(4, $phoneNumber);
            $statement->bindParam(5, $birthday);
            $statement->bindParam(6, $registerDay);
            $statement->bindParam(7, $purchase);

            $statement->execute();

            // Reset data
            $code = '';
            $name = '';
            $address = '';
            $phoneNumber = '';
            $birthday = '';
            $registerDay = '';
            $purchase = '';
        } else {
            $errors[] = 'Mã khách hàng đã tồn tại';
        }
    }
}
include_once('../../config.php');
require_once VIEW_HEADER;
?>
<div class="wrap">

    <h1>Customer CRUD</h1>
    <p>
        <a href="index.php" type="button" class="btn btn-sm btn-secondary ">Back</a>
    </p>

    <?php require_once('../../assets/views/forms/formCreateCustomer.php') ?>

</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>