<?php
require_once('../../conn.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $billNumber = $_POST['billNumber'] ?? null;

    // update to database table
    $statement = $conn->prepare("DELETE FROM hoadon WHERE sohd = :sohd");

    $statement->bindValue(':sohd', $billNumber);

    $statement->execute();
    header('Location: index.php');
}
