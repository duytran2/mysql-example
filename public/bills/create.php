<?php
require_once('../../conn.php');

$errors = [];

// define empty data
// hoadon table
$billNumber = '';
$createDate = '';
$customerCode = '';
$staffCode = '';
$invoiceValue = '';

// cthd table
$quantity = '';
// $product = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once('../../assets/function/bills/validate.php');

    $statement = $conn->prepare("SELECT * FROM hoadon WHERE sohd = :sohd");
    $statement->bindValue(':sohd', $billNumber);
    $statement->execute();

    $row = $statement->fetch(PDO::FETCH_ASSOC);

    if ($row) {
        $errors[] = 'Số hóa đơn đã tồn tại';
    }

    // if no error
    if (empty($errors)) {

        // insert to database table
        $statement = $conn->prepare("INSERT INTO hoadon (sohd, nghd, makh, manv, trigia) VALUES (:sohd, :nghd, :makh, :manv, :trigia)");

        $statement->bindValue(':sohd', $billNumber);
        $statement->bindValue(':nghd', $createDate);
        $statement->bindValue(':makh', $customerCode);
        $statement->bindValue(':manv', $staffCode);
        $statement->bindValue(':trigia', $invoiceValue);

        $statement->execute();


        foreach ($products as $product) {
            if ($quantity[$product] != 0) {
                $statement = $conn->prepare("INSERT INTO cthd (sohd, masp, sl) VALUES (:sohd, :masp, :sl);");

                $statement->bindValue(':sohd', $billNumber);
                $statement->bindValue(':masp', $product);
                $statement->bindValue(':sl', $quantity[$product]);

                $statement->execute();
            }
        }

        // Reset data
        $billNumber = '';
        $createDate = '';
        $customerCode = '';
        $staffCode = '';
        $invoiceValue = '';
        $quantity = '';
        $productCode = '';
    }
}
include_once('../../config.php');
require_once VIEW_HEADER;
?>
<div class="wrap">

    <h1>Hóa đơn CRUD</h1>
    <p>
        <a href="index.php" type="button" class="btn btn-sm btn-secondary ">Back</a>
    </p>

    <?php require_once('../../assets/views/forms/formCreateBill.php') ?>

</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>