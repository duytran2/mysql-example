<?php
require_once('../../config.php');
$bill = require DIR_FUNCTION . '/bills/getBillById.php';
$billDetail = require DIR_FUNCTION . '/bills/getBillDetailById.php';

$errors = [];

// define empty data
// hoadon table
$billNumber = $bill['sohd'];
$createDate = $bill['nghd'];
$customerCode = $bill['tenkh'];
$staffCode = $bill['tennv'];
$invoiceValue = $bill['trigia'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once('../../assets/function/bills/validate.php');

    // if no error
    if (empty($errors)) {

        // insert to database table
        $statement = $conn->prepare("UPDATE hoadon SET nghd = :nghd, makh = :makh, manv = :manv, trigia = :trigia WHERE sohd = :sohd");
        $statement->bindValue(':sohd', $billNumber);
        $statement->bindValue(':nghd', $createDate);
        $statement->bindValue(':makh', $customerCode);
        $statement->bindValue(':manv', $staffCode);
        $statement->bindValue(':trigia', $invoiceValue);
        $statement->execute();

        // delete all bill WHERE sohd
        $statement = $conn->prepare("DELETE FROM cthd WHERE sohd = :sohd;");
        $statement->bindValue(':sohd', $billNumber);
        $statement->execute();

        if ($products) {
            foreach ($products as $product) {
                if ($quantity[$product] != 0) {
                    // insert product to cthd
                    $statement = $conn->prepare("INSERT INTO cthd (sohd, masp, sl) VALUES (:sohd, :masp, :sl);");

                    $statement->bindValue(':sl', $quantity[$product]);
                    $statement->bindValue(':sohd', $billNumber);
                    $statement->bindValue(':masp', $product);

                    $statement->execute();
                } else {
                }
            }
        }

        // reload hoadon + cthd to display
        $bill = require DIR_FUNCTION . '/bills/getBillById.php';
        $billDetail = require DIR_FUNCTION . '/bills/getBillDetailById.php';
    }
}
include_once('../../config.php');
require_once VIEW_HEADER;
?>
<div class="wrap">

    <h1>Hóa đơn CRUD</h1>
    <p>
        <a href="index.php" type="button" class="btn btn-sm btn-secondary ">Back</a>
    </p>

    <?php require_once('../../assets/views/forms/formCreateBill.php') ?>

</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>