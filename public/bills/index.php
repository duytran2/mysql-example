<?php
$bills = require_once('../../assets/function/bills/getAllBills.php');
include_once('../../config.php');
require_once VIEW_HEADER;
?>

<h1>Hóa đơn CRUD</h1>
<p>
    <a href="create.php" type="button" class="btn btn-sm btn-success">New</a>
    <a href="../index.php" type="button" class="btn btn-sm btn-secondary">Back</a>
</p>
<table class="table">
    <thead>
        <tr>
            <th scope="col">Mã hóa đơn</th>
            <th scope="col">Tên khách hàng</th>
            <th scope="col">Tên nhân viên</th>
            <th scope="col">Ngày mua</th>
            <th scope="col">Trị giá</th>
            <th scope="col">Thao tác</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($bills as $bill) : ?>
            <tr>
                <th scope="row"><?php echo $bill['sohd'] ?></th>
                <td><?php echo $bill['tenkh'] ?></td>
                <td><?php echo $bill['tennv'] ?></td>
                <td><?php echo date_format(date_create($bill['nghd']), 'd/m/Y'); ?></td>
                <td><?php echo number_format($bill['trigia'], 0, ',', '.') ?>đ</td>
                <td>
                    <a href="update.php?id=<?php echo $bill['sohd'] ?>" type="button" class="btn btn-sm btn-outline-warning">Sửa</a>
                    <form style="display: inline-block;" action="delete.php" method="POST">
                        <input type="hidden" name="billNumber" value="<?php echo $bill['sohd'] ?>">
                        <button href="#" type="submit" class="btn btn-sm btn-outline-danger">Xóa</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>