<?php
require_once('../../conn.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $code = $_POST['code'] ?? null;

    // update to database table
    $statement = $conn->prepare("DELETE FROM sanpham WHERE masp = :masp");

    $statement->bindValue(':masp', $code);

    $statement->execute();
    header('Location: index.php');
}
