<?php
$product = require_once('../../assets/function/products/getProductById.php');

$errors = [];

// define empty data
$code = $product['masp'];
$title = $product['tensp'];
$unit = $product['dvt'];
$country = $product['nuocsx'];
$price = $product['gia'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once('../../assets/function/products/validate.php');

    // if no error
    if (empty($errors)) {

        // update to database table
        $statement = $conn->prepare("UPDATE sanpham SET tensp = :tensp, dvt = :dvt, gia = :gia, nuocsx = :nuocsx WHERE masp = :masp");

        $statement->bindValue(':tensp', $title);
        $statement->bindValue(':dvt', $unit);
        $statement->bindValue(':nuocsx', $country);
        $statement->bindValue(':gia', $price);
        $statement->bindValue(':masp', $code);

        $statement->execute();
    }
}
include_once('../../config.php');
require_once VIEW_HEADER;
?>
<div class="wrap">

    <h1>Product CRUD</h1>
    <p>
        <a href="index.php" type="button" class="btn btn-sm btn-secondary ">Back</a>
    </p>

    <?php require_once('../../assets/views/forms/formCreateProduct.php') ?>

</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>