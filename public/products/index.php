<?php
include_once('../../config.php');
$products = require_once('../../assets/function/products/getAllProduct.php');
require_once VIEW_HEADER;

?>

<h1>Sản phẩm CRUD</h1>
<p>
    <a href="create.php" type="button" class="btn btn-sm btn-success">New</a>
    <a href="../index.php" type="button" class="btn btn-sm btn-secondary ">Back</a>
</p>
<table class="table">
    <thead>
        <tr>
            <th scope="col">Mã Sản phẩm</th>
            <th scope="col">Tên Sản phẩm</th>
            <th scope="col">Đơn vị tính</th>
            <th scope="col">Nước sản xuất</th>
            <th scope="col">Giá</th>
            <th scope="col">Thao tác</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($products as $product) : ?>
            <tr>
                <th scope="row"><?php echo $product['masp'] ?></th>
                <td><?php echo $product['tensp'] ?></td>
                <td><?php echo $product['dvt'] ?></td>
                <td><?php echo $product['nuocsx'] ?></td>
                <td><?php echo number_format($product['gia'], 0, ',', '.') ?>đ</td>
                <td>
                    <a href="update.php?id=<?php echo $product['masp'] ?>" type="button" class="btn btn-sm btn-outline-warning">Sửa</a>
                    <form style="display: inline-block;" action="delete.php" method="POST">
                        <input type="hidden" name="code" value="<?php echo $product['masp'] ?>">
                        <button href="#" type="submit" class="btn btn-sm btn-outline-danger">Xóa</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>