<?php
include_once('../config.php');
require_once VIEW_HEADER;

?>

<h1>SQL Example</h1>
<p>
    <a href="<?php echo URL_PUBLIC . 'products/index.php'; ?>" type="button" class="btn btn-sm btn-success">Sản phẩm</a>
    <a href="<?php echo URL_PUBLIC . 'customers/index.php'; ?>" type="button" class="btn btn-sm btn-success">Khách hàng</a>
    <a href="<?php echo URL_PUBLIC . 'bills/index.php'; ?>" type="button" class="btn btn-sm btn-success">Hóa đơn</a>
    <a href="<?php echo URL_PUBLIC . 'staffs/index.php'; ?>" type="button" class="btn btn-sm btn-success">Nhân viên</a>
</p>


<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>